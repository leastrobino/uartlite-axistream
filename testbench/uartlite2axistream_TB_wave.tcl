set dut ""

set signals { \
  {"AXI PS"          {s_axi4lite_aclk s_axi4lite_aresetn s_axi4lite_awaddr s_axi4lite_wdata s_axi4lite_wvalid s_axi4lite_bvalid s_axi4lite_araddr s_axi4lite_arvalid s_axi4lite_rdata s_axi4lite_rvalid}} \
  {"AXI PCIe slave"  {s_axi4stream_aclk s_axi4stream_aresetn s_axi4stream_tdata s_axi4stream_tvalid s_axi4stream_tready}} \
  {"AXI PCIe master" {m_axi4stream_aclk m_axi4stream_aresetn m_axi4stream_tdata m_axi4stream_tvalid m_axi4stream_tready m_axi4stream_tlast}} \
  {"RX FIFO"         {rx_fifo_do rx_fifo_wren rx_fifo_rden rx_fifo_empty rx_fifo_full rx_fifo_rst}} \
  {"TX FIFO"         {tx_fifo_di tx_fifo_wren tx_fifo_rden tx_fifo_empty tx_fifo_full tx_fifo_rst}} \
  {"IRQ"             {irq_enabled uartlite_irq_o pcie_irq_req_o pcie_irq_ack_i}} \
}

set radix_bin {}
set radix_dec {}
set radix_hex {}

foreach group $signals {
  gtkwave::/Edit/Insert_Comment [lindex $group 0]
  foreach item [lindex $group 1] {
    set signal $item
    if {[string first "." $item] == 0} {
      set item [string range $item 1 end]
    } elseif {$dut != ""} {
      set item $dut.$item
    }
    gtkwave::addSignalsFromList $item
    if {[lsearch -exact $radix_bin $signal] != -1} {
      gtkwave::/Edit/Data_Format/Binary
    } elseif {[lsearch -exact $radix_dec $signal] != -1} {
      gtkwave::/Edit/Data_Format/Decimal
    } elseif {[lsearch -exact $radix_hex $signal] != -1} {
      gtkwave::/Edit/Data_Format/Hex
    }
  }
}

gtkwave::/Edit/UnHighlight_All
gtkwave::/View/Left_Justified_Signals
gtkwave::/Time/Zoom/Zoom_Full
