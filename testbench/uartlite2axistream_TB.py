# ------------------------------------------------------------------------------
#  Title      : AXI UART Lite v2.0 to AXI4-Stream bridge testbench
#  Project    :
# ------------------------------------------------------------------------------
#  File       : uartlite2axistream_TB.py
#  Author     : Lea Strobino <lea.strobino@cern.ch>
#  Company    : CERN - European Organization for Nuclear Research
#  Created    : 14/02/2021
#  Last update:
#  Platform   : cocotb 1.4
#  Standard   : Python 3
# ------------------------------------------------------------------------------
#  Description:
# ------------------------------------------------------------------------------
#  Copyright (c) 2021 CERN
#  SPDX-License-Identifier: CERN-OHL-W-2.0
# ------------------------------------------------------------------------------
#  Revisions  :
#  Date        Version  Author        Description
#  14/02/2021  1.0      Lea Strobino  Created
# ------------------------------------------------------------------------------

import cocotb
from cocotb.binary import *
from cocotb.clock import *
from cocotb.result import *
from cocotb.triggers import *
from cocotb.drivers.amba import AXI4LiteMaster

class uartlite2axistream_TB(object):

    def __init__(self, dut):

        self.dut = dut

        self.clk_ps = dut.s_axi4lite_aclk
        cocotb.fork(Clock(self.clk_ps, 10, 'ns').start())

        self.clk_pcie = dut.s_axi4stream_aclk
        cocotb.fork(Clock(self.clk_pcie, 8, 'ns').start())
        cocotb.fork(Clock(dut.m_axi4stream_aclk, 8, 'ns').start())

        self.axi_ps = AXI4LiteMaster(dut, 's_axi4lite', self.clk_ps)

        dut.s_axi4stream_tdata.setimmediatevalue(0)
        dut.s_axi4stream_tvalid.setimmediatevalue(0)
        dut.m_axi4stream_tready.setimmediatevalue(0)

        dut.link_up_i.setimmediatevalue(0)
        dut.pcie_irq_ack_i.setimmediatevalue(0)

    async def reset(self):

        # Reset for 5 clock cycles
        self.dut.s_axi4lite_aresetn.setimmediatevalue(0)
        self.dut.s_axi4stream_aresetn.setimmediatevalue(0)
        self.dut.m_axi4stream_aresetn.setimmediatevalue(0)
        await Timer(1)
        await ClockCycles(self.clk_ps, 5)

        # Run for 5 clock cycles
        self.dut.s_axi4lite_aresetn <= 1
        self.dut.s_axi4stream_aresetn <= 1
        self.dut.m_axi4stream_aresetn <= 1
        await ClockCycles(self.clk_ps, 5)

    async def ps_write(self, data):
        tx_fifo_full = 1
        while tx_fifo_full:
            stat = await self.axi_ps.read(0x08)
            tx_fifo_full = (stat >> 3) & 1
        await self.axi_ps.write(0x04, data)

    async def ps_read(self):
        rx_fifo_valid = 0
        while not rx_fifo_valid:
            stat = await self.axi_ps.read(0x08)
            rx_fifo_valid = stat & 1
        data = await self.axi_ps.read(0x00)
        return int(data)

    async def ps_reset(self):
        await self.axi_ps.write(0x0C, 0b11)

    async def ps_enable_irq(self, enable_irq=1):
        await self.axi_ps.write(0x0C, bool(enable_irq) << 4)

    async def pcie_write(self, data):
        self.dut.s_axi4stream_tdata <= data
        self.dut.s_axi4stream_tvalid <= 1
        tready = 0
        while not tready:
            await RisingEdge(self.clk_pcie)
            tready = self.dut.s_axi4stream_tready.value
        self.dut.s_axi4stream_tvalid <= 0

    async def pcie_read(self):
        self.dut.m_axi4stream_tready <= 1
        tvalid = 0
        while not tvalid:
            await RisingEdge(self.clk_pcie)
            tvalid = self.dut.m_axi4stream_tvalid.value
        self.dut.m_axi4stream_tready <= 0
        return int(self.dut.m_axi4stream_tdata.value)

@cocotb.test()
async def T00_PS_to_PCIe(dut):

    TB = uartlite2axistream_TB(dut)
    await TB.reset()

    await TB.ps_enable_irq()

    async def T00_PS_to_PCIe_write(TB):
        for i in range(16, 32):
            await TB.ps_write(i)

    async def T00_PS_to_PCIe_irq_ack(TB):
        value = 0
        while True:
            await RisingEdge(TB.clk_pcie)
            TB.dut.pcie_irq_ack_i <= (TB.dut.pcie_irq_req_o.value and not value)
            value = TB.dut.pcie_irq_req_o.value

    cocotb.fork(T00_PS_to_PCIe_write(TB))
    cocotb.fork(T00_PS_to_PCIe_irq_ack(TB))

    await ClockCycles(TB.clk_pcie, 50)
    for i in range(16, 21):
        data = await TB.pcie_read()
        if data != i:
            msg = 'Read failed (expected value: 0x{:02X}, actual value: 0x{:02X})'.format(i, data)
            raise TestFailure(msg)

    await ClockCycles(TB.clk_pcie, 2)

    for i in range(21, 32):
        data = await TB.pcie_read()
        if data != i:
            msg = 'Read failed (expected value: 0x{:02X}, actual value: 0x{:02X})'.format(i, data)
            raise TestFailure(msg)

    await ClockCycles(TB.clk_ps, 72)

@cocotb.test()
async def T01_PCIe_to_PS(dut):

    TB = uartlite2axistream_TB(dut)
    await TB.reset()

    await TB.ps_enable_irq()

    async def T01_PCIe_to_PS_write(TB):
        await RisingEdge(TB.clk_pcie)
        for i in range(16, 32):
            await TB.pcie_write(i)

    cocotb.fork(T01_PCIe_to_PS_write(TB))

    await ClockCycles(TB.clk_pcie, 50)

    for i in range(16, 32):
        data = await TB.ps_read()
        if data != i:
            msg = 'Read failed (expected value: 0x{:02X}, actual value: 0x{:02X})'.format(i, data)
            raise TestFailure(msg)

    await ClockCycles(TB.clk_pcie, 64)
