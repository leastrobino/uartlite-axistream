--------------------------------------------------------------------------------
-- Title      : AXI UART Lite v2.0 to AXI4-Stream bridge
-- Project    :
--------------------------------------------------------------------------------
-- File       : uartlite2axistream.vhd
-- Author     : Lea Strobino <lea.strobino@cern.ch>
-- Company    : CERN - European Organization for Nuclear Research
-- Created    : 09/02/2021
-- Last update: 27/02/2021
-- Platform   :
-- Standard   : VHDL'93/02
--------------------------------------------------------------------------------
-- Description:
--------------------------------------------------------------------------------
-- Copyright (c) 2021 CERN
-- SPDX-License-Identifier: CERN-OHL-W-2.0
--------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author        Description
-- 27/02/2021  1.0      Lea Strobino  Created
--------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
library UNISIM;
use UNISIM.vcomponents.all;

entity uartlite2axistream is
  generic (
    g_reset_duration            : integer range 5 to 100 := 5;
    g_tlast_on_fifo_empty       : boolean := true
  );
  port (
    -- AXI4-Lite slave
    s_axi4lite_aclk             : in  std_logic;
    s_axi4lite_aresetn          : in  std_logic;
    s_axi4lite_awvalid          : in  std_logic;
    s_axi4lite_awready          : out std_logic;
    s_axi4lite_awaddr           : in  std_logic_vector(3 downto 0);
    s_axi4lite_wvalid           : in  std_logic;
    s_axi4lite_wready           : out std_logic;
    s_axi4lite_wdata            : in  std_logic_vector(31 downto 0);
    s_axi4lite_wstrb            : in  std_logic_vector(3 downto 0);
    s_axi4lite_bvalid           : out std_logic;
    s_axi4lite_bready           : in  std_logic;
    s_axi4lite_bresp            : out std_logic_vector(1 downto 0);
    s_axi4lite_arvalid          : in  std_logic;
    s_axi4lite_arready          : out std_logic;
    s_axi4lite_araddr           : in  std_logic_vector(3 downto 0);
    s_axi4lite_rvalid           : out std_logic;
    s_axi4lite_rready           : in  std_logic;
    s_axi4lite_rdata            : out std_logic_vector(31 downto 0);
    s_axi4lite_rresp            : out std_logic_vector(1 downto 0);
    -- AXI4-Stream slave
    s_axi4stream_aclk           : in  std_logic;
    s_axi4stream_aresetn        : in  std_logic;
    s_axi4stream_tdata          : in  std_logic_vector(7 downto 0);
    s_axi4stream_tvalid         : in  std_logic;
    s_axi4stream_tready         : out std_logic;
    -- AXI4-Stream master
    m_axi4stream_aclk           : in  std_logic;
    m_axi4stream_aresetn        : in  std_logic;
    m_axi4stream_tdata          : out std_logic_vector(7 downto 0);
    m_axi4stream_tvalid         : out std_logic;
    m_axi4stream_tready         : in  std_logic;
    m_axi4stream_tlast          : out std_logic;
    -- Control logic
    link_up_i                   : in  std_logic := '1';
    uartlite_irq_o              : out std_logic;
    pcie_irq_req_o              : out std_logic;
    pcie_irq_ack_i              : in  std_logic
  );
end entity;

architecture RTL of uartlite2axistream is

  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of s_axi4lite_aclk      : signal is "xilinx.com:signal:clock:1.0 s_axi4lite_aclk CLK";
  attribute X_INTERFACE_INFO of s_axi4lite_aresetn   : signal is "xilinx.com:signal:reset:1.0 s_axi4lite_aresetn RST";
  attribute X_INTERFACE_INFO of s_axi4lite_awvalid   : signal is "xilinx.com:interface:aximm:1.0 S_AXI4LITE AWVALID";
  attribute X_INTERFACE_INFO of s_axi4lite_awready   : signal is "xilinx.com:interface:aximm:1.0 S_AXI4LITE AWREADY";
  attribute X_INTERFACE_INFO of s_axi4lite_awaddr    : signal is "xilinx.com:interface:aximm:1.0 S_AXI4LITE AWADDR";
  attribute X_INTERFACE_INFO of s_axi4lite_wvalid    : signal is "xilinx.com:interface:aximm:1.0 S_AXI4LITE WVALID";
  attribute X_INTERFACE_INFO of s_axi4lite_wready    : signal is "xilinx.com:interface:aximm:1.0 S_AXI4LITE WREADY";
  attribute X_INTERFACE_INFO of s_axi4lite_wdata     : signal is "xilinx.com:interface:aximm:1.0 S_AXI4LITE WDATA";
  attribute X_INTERFACE_INFO of s_axi4lite_wstrb     : signal is "xilinx.com:interface:aximm:1.0 S_AXI4LITE WSTRB";
  attribute X_INTERFACE_INFO of s_axi4lite_bvalid    : signal is "xilinx.com:interface:aximm:1.0 S_AXI4LITE BVALID";
  attribute X_INTERFACE_INFO of s_axi4lite_bready    : signal is "xilinx.com:interface:aximm:1.0 S_AXI4LITE BREADY";
  attribute X_INTERFACE_INFO of s_axi4lite_bresp     : signal is "xilinx.com:interface:aximm:1.0 S_AXI4LITE BRESP";
  attribute X_INTERFACE_INFO of s_axi4lite_arvalid   : signal is "xilinx.com:interface:aximm:1.0 S_AXI4LITE ARVALID";
  attribute X_INTERFACE_INFO of s_axi4lite_arready   : signal is "xilinx.com:interface:aximm:1.0 S_AXI4LITE ARREADY";
  attribute X_INTERFACE_INFO of s_axi4lite_araddr    : signal is "xilinx.com:interface:aximm:1.0 S_AXI4LITE ARADDR";
  attribute X_INTERFACE_INFO of s_axi4lite_rvalid    : signal is "xilinx.com:interface:aximm:1.0 S_AXI4LITE RVALID";
  attribute X_INTERFACE_INFO of s_axi4lite_rready    : signal is "xilinx.com:interface:aximm:1.0 S_AXI4LITE RREADY";
  attribute X_INTERFACE_INFO of s_axi4lite_rdata     : signal is "xilinx.com:interface:aximm:1.0 S_AXI4LITE RDATA";
  attribute X_INTERFACE_INFO of s_axi4lite_rresp     : signal is "xilinx.com:interface:aximm:1.0 S_AXI4LITE RRESP";
  attribute X_INTERFACE_INFO of s_axi4stream_aclk    : signal is "xilinx.com:signal:clock:1.0 s_axi4stream_aclk CLK";
  attribute X_INTERFACE_INFO of s_axi4stream_aresetn : signal is "xilinx.com:signal:reset:1.0 s_axi4stream_aresetn RST";
  attribute X_INTERFACE_INFO of s_axi4stream_tdata   : signal is "xilinx.com:interface:axis:1.0 S_AXI4STREAM TDATA";
  attribute X_INTERFACE_INFO of s_axi4stream_tvalid  : signal is "xilinx.com:interface:axis:1.0 S_AXI4STREAM TVALID";
  attribute X_INTERFACE_INFO of s_axi4stream_tready  : signal is "xilinx.com:interface:axis:1.0 S_AXI4STREAM TREADY";
  attribute X_INTERFACE_INFO of m_axi4stream_aclk    : signal is "xilinx.com:signal:clock:1.0 m_axi4stream_aclk CLK";
  attribute X_INTERFACE_INFO of m_axi4stream_aresetn : signal is "xilinx.com:signal:reset:1.0 m_axi4stream_aresetn RST";
  attribute X_INTERFACE_INFO of m_axi4stream_tdata   : signal is "xilinx.com:interface:axis:1.0 M_AXI4STREAM TDATA";
  attribute X_INTERFACE_INFO of m_axi4stream_tvalid  : signal is "xilinx.com:interface:axis:1.0 M_AXI4STREAM TVALID";
  attribute X_INTERFACE_INFO of m_axi4stream_tready  : signal is "xilinx.com:interface:axis:1.0 M_AXI4STREAM TREADY";
  attribute X_INTERFACE_INFO of m_axi4stream_tlast   : signal is "xilinx.com:interface:axis:1.0 M_AXI4STREAM TLAST";
  attribute X_INTERFACE_INFO of uartlite_irq_o       : signal is "xilinx.com:signal:interrupt:1.0 uartlite_irq_o INTERRUPT";

  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of s_axi4lite_aclk      : signal is "ASSOCIATED_BUSIF S_AXI4LITE";
  attribute X_INTERFACE_PARAMETER of s_axi4lite_aresetn   : signal is "POLARITY ACTIVE_LOW";
  attribute X_INTERFACE_PARAMETER of s_axi4stream_aclk    : signal is "ASSOCIATED_BUSIF S_AXI4STREAM";
  attribute X_INTERFACE_PARAMETER of s_axi4stream_aresetn : signal is "POLARITY ACTIVE_LOW";
  attribute X_INTERFACE_PARAMETER of m_axi4stream_aclk    : signal is "ASSOCIATED_BUSIF M_AXI4STREAM";
  attribute X_INTERFACE_PARAMETER of m_axi4stream_aresetn : signal is "POLARITY ACTIVE_LOW";
  attribute X_INTERFACE_PARAMETER of uartlite_irq_o       : signal is "SENSITIVITY EDGE_RISING";

  signal ctrl_rst_rx_fifo       : std_logic;
  signal ctrl_rst_tx_fifo       : std_logic;
  signal ctrl_enable_irq        : std_logic;
  signal ctrl_wr                : std_logic;
  signal ctrl_wr_d              : std_logic_vector(g_reset_duration-1 downto 0);
  signal rst_rx_fifo            : std_logic;
  signal rst_tx_fifo            : std_logic;
  signal irq_enabled            : std_logic;
  signal link_up_d              : std_logic_vector(1 downto 0);

  signal rx_fifo_do             : std_logic_vector(7 downto 0);
  signal rx_fifo_rd             : std_logic;
  signal rx_fifo_wren           : std_logic;
  signal rx_fifo_rden           : std_logic;
  signal rx_fifo_empty          : std_logic;
  signal rx_fifo_full           : std_logic;
  signal rx_fifo_full_d         : std_logic_vector(1 downto 0);
  signal rx_fifo_valid          : std_logic;
  signal rx_fifo_rst            : std_logic;

  signal tx_fifo_di             : std_logic_vector(7 downto 0);
  signal tx_fifo_wr             : std_logic;
  signal tx_fifo_wren           : std_logic;
  signal tx_fifo_rden           : std_logic;
  signal tx_fifo_empty          : std_logic;
  signal tx_fifo_empty_d        : std_logic_vector(1 downto 0);
  signal tx_fifo_full           : std_logic;
  signal tx_fifo_full_link_up   : std_logic;
  signal tx_fifo_valid          : std_logic;
  signal tx_fifo_valid_d        : std_logic;
  signal tx_fifo_rst            : std_logic;

  signal m_axi4stream_tvalid_s  : std_logic;

  signal reserved_0, reserved_1 : std_logic_vector(63 downto 8);

  component uartlite_axi4lite is
    port (
      aclk                      : in  std_logic;
      areset_n                  : in  std_logic;
      awvalid                   : in  std_logic;
      awready                   : out std_logic;
      awaddr                    : in  std_logic_vector(3 downto 2);
      awprot                    : in  std_logic_vector(2 downto 0) := "000";
      wvalid                    : in  std_logic;
      wready                    : out std_logic;
      wdata                     : in  std_logic_vector(31 downto 0);
      wstrb                     : in  std_logic_vector(3 downto 0);
      bvalid                    : out std_logic;
      bready                    : in  std_logic;
      bresp                     : out std_logic_vector(1 downto 0);
      arvalid                   : in  std_logic;
      arready                   : out std_logic;
      araddr                    : in  std_logic_vector(3 downto 2);
      arprot                    : in  std_logic_vector(2 downto 0) := "000";
      rvalid                    : out std_logic;
      rready                    : in  std_logic;
      rdata                     : out std_logic_vector(31 downto 0);
      rresp                     : out std_logic_vector(1 downto 0);
      rx_fifo_i                 : in  std_logic_vector(7 downto 0);
      rx_fifo_rd_o              : out std_logic;
      tx_fifo_o                 : out std_logic_vector(7 downto 0);
      tx_fifo_wr_o              : out std_logic;
      stat_rx_fifo_valid_i      : in  std_logic;
      stat_rx_fifo_full_i       : in  std_logic;
      stat_tx_fifo_empty_i      : in  std_logic;
      stat_tx_fifo_full_i       : in  std_logic;
      stat_irq_enabled_i        : in  std_logic;
      stat_overrun_error_i      : in  std_logic := '0';
      stat_frame_error_i        : in  std_logic := '0';
      stat_parity_error_i       : in  std_logic := '0';
      ctrl_rst_rx_fifo_o        : out std_logic;
      ctrl_rst_tx_fifo_o        : out std_logic;
      ctrl_enable_irq_o         : out std_logic;
      ctrl_wr_o                 : out std_logic
    );
  end component;

begin

  -- AXI4-Lite slave interface
  cmp_uartlite_axi4lite : component uartlite_axi4lite
  port map (
    aclk                        => s_axi4lite_aclk,
    areset_n                    => s_axi4lite_aresetn,
    awvalid                     => s_axi4lite_awvalid,
    awready                     => s_axi4lite_awready,
    awaddr                      => s_axi4lite_awaddr(3 downto 2),
    wvalid                      => s_axi4lite_wvalid,
    wready                      => s_axi4lite_wready,
    wdata                       => s_axi4lite_wdata,
    wstrb                       => s_axi4lite_wstrb,
    bvalid                      => s_axi4lite_bvalid,
    bready                      => s_axi4lite_bready,
    bresp                       => s_axi4lite_bresp,
    arvalid                     => s_axi4lite_arvalid,
    arready                     => s_axi4lite_arready,
    araddr                      => s_axi4lite_araddr(3 downto 2),
    rvalid                      => s_axi4lite_rvalid,
    rready                      => s_axi4lite_rready,
    rdata                       => s_axi4lite_rdata,
    rresp                       => s_axi4lite_rresp,
    rx_fifo_i                   => rx_fifo_do,
    rx_fifo_rd_o                => rx_fifo_rd,
    tx_fifo_o                   => tx_fifo_di,
    tx_fifo_wr_o                => tx_fifo_wr,
    stat_rx_fifo_valid_i        => rx_fifo_valid,
    stat_rx_fifo_full_i         => rx_fifo_full_d(1),
    stat_tx_fifo_empty_i        => tx_fifo_empty_d(1),
    stat_tx_fifo_full_i         => tx_fifo_full_link_up,
    stat_irq_enabled_i          => irq_enabled,
    ctrl_rst_rx_fifo_o          => ctrl_rst_rx_fifo,
    ctrl_rst_tx_fifo_o          => ctrl_rst_tx_fifo,
    ctrl_enable_irq_o           => ctrl_enable_irq,
    ctrl_wr_o                   => ctrl_wr
  );
  tx_fifo_full_link_up          <= tx_fifo_full and link_up_d(1);

  -- Control register
  process (s_axi4lite_aclk) begin
    if rising_edge(s_axi4lite_aclk) then
      if s_axi4lite_aresetn = '0' then
        rst_rx_fifo <= '0';
        rst_tx_fifo <= '0';
        irq_enabled <= '0';
        ctrl_wr_d   <= (others => '0');
      else
        if ctrl_wr = '1' and ctrl_rst_rx_fifo = '1' then
          rst_rx_fifo <= '1';
        elsif ctrl_wr_d(ctrl_wr_d'left) = '1' then
          rst_rx_fifo <= '0';
        end if;
        if ctrl_wr = '1' and ctrl_rst_tx_fifo = '1' then
          rst_tx_fifo <= '1';
        elsif ctrl_wr_d(ctrl_wr_d'left) = '1' then
          rst_tx_fifo <= '0';
        end if;
        if ctrl_wr = '1' then
          irq_enabled <= ctrl_enable_irq;
        end if;
        ctrl_wr_d <= ctrl_wr_d(ctrl_wr_d'left-1 downto 0) & ctrl_wr;
      end if;
    end if;
  end process;

  -- RX FIFO
  cmp_rx_fifo : component FIFO36E1
  generic map (
    DATA_WIDTH                  => 9,
    DO_REG                      => 1,
    EN_ECC_READ                 => false,
    EN_ECC_WRITE                => false,
    EN_SYN                      => false,
    FIFO_MODE                   => "FIFO36",
    FIRST_WORD_FALL_THROUGH     => true
  )
  port map (
    DI(63 downto 8)             => (others => '0'),
    DI(7 downto 0)              => s_axi4stream_tdata,
    DO(63 downto 8)             => reserved_0,
    DO(7 downto 0)              => rx_fifo_do,
    WRCLK                       => s_axi4stream_aclk,
    WREN                        => rx_fifo_wren,
    RDCLK                       => s_axi4lite_aclk,
    RDEN                        => rx_fifo_rden,
    EMPTY                       => rx_fifo_empty,
    FULL                        => rx_fifo_full,
    RST                         => rx_fifo_rst,
    DIP                         => (others => '0'),
    INJECTDBITERR               => '0',
    INJECTSBITERR               => '0',
    REGCE                       => '0',
    RSTREG                      => '0'
  );
  rx_fifo_wren                  <= s_axi4stream_tvalid and not rx_fifo_full and not rx_fifo_rst;
  rx_fifo_rden                  <= rx_fifo_rd and not rx_fifo_empty and not rx_fifo_rst;
  rx_fifo_valid                 <= not rx_fifo_empty;
  rx_fifo_rst                   <= not s_axi4lite_aresetn or not s_axi4stream_aresetn or rst_rx_fifo;

  -- TX FIFO
  cmp_tx_fifo : component FIFO36E1
  generic map (
    DATA_WIDTH                  => 9,
    DO_REG                      => 1,
    EN_ECC_READ                 => false,
    EN_ECC_WRITE                => false,
    EN_SYN                      => false,
    FIFO_MODE                   => "FIFO36",
    FIRST_WORD_FALL_THROUGH     => not g_tlast_on_fifo_empty
  )
  port map (
    DI(63 downto 8)             => (others => '0'),
    DI(7 downto 0)              => tx_fifo_di,
    DO(63 downto 8)             => reserved_1,
    DO(7 downto 0)              => m_axi4stream_tdata,
    WRCLK                       => s_axi4lite_aclk,
    WREN                        => tx_fifo_wren,
    RDCLK                       => m_axi4stream_aclk,
    RDEN                        => tx_fifo_rden,
    EMPTY                       => tx_fifo_empty,
    FULL                        => tx_fifo_full,
    RST                         => tx_fifo_rst,
    DIP                         => (others => '0'),
    INJECTDBITERR               => '0',
    INJECTSBITERR               => '0',
    REGCE                       => '0',
    RSTREG                      => '0'
  );
  tx_fifo_wren                  <= tx_fifo_wr and not tx_fifo_full and not tx_fifo_rst;
  tx_fifo_rden                  <= m_axi4stream_tready and not tx_fifo_empty and not tx_fifo_rst;
  tx_fifo_valid                 <= not tx_fifo_empty;
  tx_fifo_rst                   <= not s_axi4lite_aresetn or not m_axi4stream_aresetn or rst_tx_fifo;

  -- Clock domain crossing
  --   rx_fifo_empty and tx_fifo_full are synchronous to s_axi4lite_aclk   (RX FIFO RDCLK and TX FIFO WRCLK),
  --   rx_fifo_full                   is  synchronous to s_axi4stream_aclk (RX FIFO WRCLK),
  --   tx_fifo_empty                  is  synchronous to m_axi4stream_aclk (TX FIFO RDCLK).
  process (s_axi4lite_aclk) begin
    if rising_edge(s_axi4lite_aclk) then
      rx_fifo_full_d  <= rx_fifo_full_d(0)  & rx_fifo_full;
      tx_fifo_empty_d <= tx_fifo_empty_d(0) & tx_fifo_empty;
      link_up_d       <= link_up_d(0)       & link_up_i;
    end if;
  end process;

  -- AXI4-Stream slave interface
  s_axi4stream_tready           <= not rx_fifo_full;

  -- AXI4-Stream master interface
  gen_tlast_true : if g_tlast_on_fifo_empty generate
    process (m_axi4stream_aclk) begin
      if rising_edge(m_axi4stream_aclk) then
        if m_axi4stream_aresetn = '0' then
          m_axi4stream_tvalid_s <= '0';
        elsif m_axi4stream_tready = '1' then
          m_axi4stream_tvalid_s <= not tx_fifo_empty;
        end if;
      end if;
    end process;
    m_axi4stream_tvalid         <= m_axi4stream_tvalid_s;
    m_axi4stream_tlast          <= m_axi4stream_tvalid_s and tx_fifo_empty;
  end generate;
  gen_tlast_false : if not g_tlast_on_fifo_empty generate
    m_axi4stream_tvalid         <= not tx_fifo_empty;
    m_axi4stream_tlast          <= '0';
  end generate;

  -- Uartlite interrupt
  process (s_axi4lite_aclk)
    variable v_rx_fifo_valid, v_tx_fifo_empty : std_logic;
  begin
    if rising_edge(s_axi4lite_aclk) then
      if s_axi4lite_aresetn = '0' then
        v_rx_fifo_valid := '0';
        v_tx_fifo_empty := '0';
        uartlite_irq_o  <= '0';
      else
        uartlite_irq_o  <= '0';
        if irq_enabled = '1' then
          if v_rx_fifo_valid = '0' and rx_fifo_valid = '1' then  -- rising edge
            uartlite_irq_o <= '1';
          end if;
          if v_tx_fifo_empty = '0' and tx_fifo_empty_d(1) = '1' then  -- rising edge
            uartlite_irq_o <= '1';
          end if;
        end if;
        v_rx_fifo_valid := rx_fifo_valid;
        v_tx_fifo_empty := tx_fifo_empty_d(1);
      end if;
    end if;
  end process;

  -- PCIe interrupt
  process (m_axi4stream_aclk) begin
    if rising_edge(m_axi4stream_aclk) then
      if m_axi4stream_aresetn = '0' then
        tx_fifo_valid_d <= '0';
        pcie_irq_req_o  <= '0';
      else
        tx_fifo_valid_d <= tx_fifo_valid;
        if tx_fifo_valid = '1' and tx_fifo_valid_d = '0' then  -- rising edge
          pcie_irq_req_o <= '1';
          if pcie_irq_ack_i = '1' then
            tx_fifo_valid_d <= '0';
            pcie_irq_req_o  <= '0';
          end if;
        elsif pcie_irq_ack_i = '1' then
          pcie_irq_req_o <= '0';
        end if;
      end if;
    end if;
  end process;

end architecture;
